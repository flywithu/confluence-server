FROM atlassian/confluence-server:latest
MAINTAINER flywithu@gmail.com

COPY mysql-connector-java-5.1.44-bin.jar /opt/atlassian/confluence/confluence/WEB-INF/lib
